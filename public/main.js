function clickButton(){

  var recherche = document.getElementById("recherche").value;
  var nbSuggestion = document.getElementById("nbSuggest").value;

  var xmlhttp=new XMLHttpRequest();

  xmlhttp.onreadystatechange=function()
  {
      if (xmlhttp.readyState==4 && xmlhttp.status>=200 && xmlhttp.status<300)
      {
       var object = JSON.parse(xmlhttp.responseText);
       var final = "<ul>";

       for (var i = 0; i < parseInt(nbSuggestion); i++) {
         final += "<li><a href=" + `${object[3][i]}` + ">" + `${object[1][i]}` + "</a></li>";
       }

       final += "</ul>";

       document.getElementById("resultat").innerHTML=final;
      }
  }

  xmlhttp.open("GET","http://localhost:1234/api?action=opensearch&format=json&formatversion=2&search=" + `${recherche}` + "&namespace=0&limit=" + `${nbSuggestion}` + "&suggest=true",true);
  xmlhttp.send();
}
